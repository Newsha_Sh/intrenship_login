import 'package:flutter/material.dart';
import 'package:newshaproject/pages/home.dart';
import 'package:newshaproject/pages/login.dart';
import 'package:newshaproject/pages/register.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      darkTheme: ThemeData(
          backgroundColor: Colors.blueAccent,
          iconTheme: IconThemeData(color: Colors.deepOrange)),
      title: "Welcome",
      theme: ThemeData.dark(),
      initialRoute: '/',
      routes: {
        '/': (context) => Home(),
        '/login': (context) => Login(),
        '/register': (context) => Register(),
      },
    );
  }
}
