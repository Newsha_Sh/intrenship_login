import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  List<DropdownMenuItem<int>> _days = [for (int i = 1; i < 32; i++) i]
      .map(
        (_) => DropdownMenuItem<int>(
          child: Text(_.toString()),
          value: _,
        ),
      )
      .toList();

  int _day;
  List<DropdownMenuItem<int>> _year = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orangeAccent,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Register'),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                icon: Icon(Icons.insert_emoticon),
                hintText: 'Please Enter Your FullName',
              ),
            ),
            TextField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                icon: Icon(Icons.email),
                hintText: 'Please Enter Your Email Address',
              ),
            ),
            TextField(
              maxLength: 2,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  icon: Icon(Icons.arrow_forward),
                  hintText: 'Please Enter Your Age',
                  counterText: ""),
            ),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                icon: Icon(Icons.person_add),
                hintText: 'PLease Choose A Username',
              ),
            ),
            TextField(
              obscureText: true,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                icon: Icon(Icons.lock_outline),
                hintText: 'Please Choose A Password',
              ),
            ),
            TextField(
              obscureText: true,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                icon: Icon(Icons.check),
                hintText: 'Please Confirm Your Password',
              ),
            ),
            DropdownButton(
              items: _days,
              value: _day,
              hint: Text('day'),
              onChanged: (int day) => setState(() => _day = day),
            ),
            RawMaterialButton(
              onPressed: () {},
              child: Text('Register'),
              shape: RoundedRectangleBorder(),
              fillColor: Colors.blueAccent,
              splashColor: Colors.indigo,
            ),
          ],
        ),
      ),
    );
  }
}
