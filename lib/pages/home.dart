import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Homestate();
}

class _Homestate extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orangeAccent,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Internship'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RawMaterialButton(
              onPressed: () => Navigator.of(context).pushNamed('/login'),
              shape: StadiumBorder(),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(Icons.person),
                  Container(width: 8),
                  Text('login'),
                ],
              ),
            ),
            RawMaterialButton(
              onPressed: () => Navigator.of(context).pushNamed('/register'),
              shape: StadiumBorder(),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(Icons.person_add),
                  Container(width: 8),
                  Text('register'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
