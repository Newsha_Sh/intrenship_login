import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orangeAccent,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Login'),
      ),
      body: Container(
        margin: const EdgeInsets.only(left: 16, right: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              decoration:
                  InputDecoration(hintText: 'Please Enter Your Username'),
            ),
            TextField(
              decoration:
                  InputDecoration(hintText: 'Please Enter Your Password'),
            ),
            RawMaterialButton(
              onPressed: () {},
              child: Text('Ready To Go'),
              shape: RoundedRectangleBorder(),
              fillColor: Colors.blueAccent,
              splashColor: Colors.indigo,
            ),
          ],
        ),
      ),
    );
  }
}
